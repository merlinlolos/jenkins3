// Un commentaire sur une ligne
/*message = "Bienvenue dans Javascript";
alert(message);*/

// Les tableaux de données
users = [ "John", "Olivia", "Jack"];

message = "Bienvenue dans Javascript " + users[1];
console.log(message);

// Les tableaux peuvent se composer de tous types de données
account = [ 12345, "Olivia", "Jones", true ];
console.log("Numéro d'employé : " + account [0] + "[" + typeof(account [0]) + "]");
console.log("Prénom : " + account[1] + "[" + typeof(account [1]) + "]");
console.log("Nom : " + account[2] + "[" + typeof(account [2]) + "]");
console.log("Statut : " + account[3] + "[" + typeof(account [3]) + "]");

// La propriété .length des tableaux
console.log("Longueur tableau : " + account.length);

// Utilisation des fonctions
function showDryMessage() {
    console.log("Don't repeat yourself");
}

showDryMessage();
console.log("Another line");
showDryMessage();

// Déclaration de variable avec let
let season = "spring";
console.log("Saison : " + season);
season = "summer";
console.log("Saison : " + season);
console.log("********************************************************") // séparateur visuel

// let season = "winter"; = message d'erreur car let season à déjà une valeur

// Déclaration de constante avec const
const p1 = 3.1416;

function getCircleArea(radius) {
    let area = p1 * (radius ** 2);
    console.log("Aire : " + area);
}

getCircleArea(20);
console.log("********************************************************")

// Portée locale et globale des variables
function showFruit() {
    let fruit = "Lemon";
    console.log("Fruit : " + fruit);
}

let fruit = "Apple";
console.log("Fruit : " + fruit);
showFruit();    // Ici on appelle la fonction pour qu'elle puisse être exécutée
console.log("********************************************************")

// Passage de tableau lors de l'appel d'une fonction
function showCarData(car) {
    console.log ("Marque : " + car[0]);
    console.log ("Modèle : " + car[1]);
    console.log ("Carburant : " + car[2]);
    console.log ("Disponibilité : " + car[3]);
    console.log ("Longueur tableau : " + car.length);
}

let car = [ "Toyota", "Yaris", "Hybride", true]
showCarData(car);

car = [ "Volkswagen", "Polo", "Essence", true]
showCarData(car);
console.log("********************************************************")

// Utilisation d'une fonction avec valeur retournée (ici TVA)
const vatRate = 21;

function getVatIncPrice(price) {
    let vatIncPrice = price + ((price / 100) * vatRate);
    return vatIncPrice;
}

function printSeperator() { // Création d'une fonction de séparation
    console.log("********************************************************")
}

let vatIncPrice = getVatIncPrice (200);
console.log ("Prix TTC : " + vatIncPrice);
printSeperator(); // fonction de séparation

// Utilisation de l'instruction if (séparateur)
if (2 * 3 ==6) {
    console.log("2 x 3 est bien égal à 6");
}
printSeperator();

// Vérification de la valeur "true" avec "if"
let active = true;
if (active) {
    console.log('La valeur de active est "true"');
}
printSeperator();

// Utilisation de "else"
let result = 15;
if (result > 100) {
    console.log("Résultat supérieur à 100");
} 
else {
    console.log("Résultat inférieur ou égal à 100");  
}
printSeperator();

// Imbrication d'instructions if/else
let stock = 20;
if(stock > 50) {
    console.log("Résultat supérieur à 50" );
} else if (stock > 5 ) {
    console.log("Résultat supérieur à 5" );
} else {
    console.log("Résultat trop petit" );
}
printSeperator();

// Utilisation de l'opérateur logique && (ET)
let vegetable1 = "carotte";
let vegetable2 = "salade";

if ((vegetable1 == "carotte") && (vegetable2 == "salade")) {
    console.log("Les deux conditions sont respectées" );
} else {
    console.log("Les deux conditions ne sont PAS respectées" );
}
printSeperator();

// Utilisation de l'opérateur logique || (OU)
let fruit1 = "pomme";
let fruit2 = "citron";

if ((fruit1 =="pomme") || (fruit2 == "citron")) {
    console.log("Au moins 1 condition est respectées" );
}
printSeperator();

 // Les instructions switch/case
 let color = 'vert'; // C'est ici qu'on modifie la couleur
 
 switch (color) {
    case 'rouge':
        console.log("Vous avez choisi le rouge" );
        break;
    case 'vert':
        console.log("Vous avez choisi le vert" );
        break;
    case 'bleu':
        console.log("Vous avez choisi le bleu" );
        break;
    default:
        console.log("Choisissez une couleur primaire" );
 }
 printSeperator();

 // Regrouper plusieurs cas avec switch/case
 let item = 'chien';
 
 switch (item) {
    case 'chat':
    case 'chien':
    case 'oiseau':
        console.log('Vous avez choisi un animal');
        break;
    default:
        console.log("Choisissez un animal");
 }
 printSeperator();

 // Nombre d'intéractions données avec "for"
 let people = ["James","Mike","Bill"]

 for (let i = 0; i < people.length; i++) {
    console.log("Welcome to loops " + people[i]);
 }
 printSeperator();

// Itérations exécutées tant que la condition renvoie "true"
//    console.log (Math.random());
//    console.log (Math.floor(5.6789));

    function getRandomNumber() {
        let randomNumber = Math.floor(Math.random() * 10);
        return randomNumber; 
    }

let randomNumber = getRandomNumber();
while(randomNumber !== 7) {
    console.log(randomNumber + " n'est pas égal à 7");
    randomNumber = getRandomNumber(); 
}
console.log(randomNumber + " est égal à 7");
printSeperator();

// Une instruction while le code ne sera jamais exécuté
let counter = 10;
while (counter < 5) {
    console.log(counter + " est plus petit que 5");
    counter++;
}
console.log("Le code ne sera jamais exécuté");
printSeperator();

 // Une instruction do/while sont exécutées au moins une fois
 let count = 1;

 do {
    console.log(count);
    count++;
} while (count < 1)
printSeperator();

// Utilisation de l'instruction "continue" (saying = dicton)
let saying = "Qui resteint ses besoins sera d'autant plus libre.";
let vowels = ["a","e","i","o","u","y"];

result = vowels.indexOf(saying.charAt(2));
console.log(result);

let vowelsCounter = 0;
for (let i = 0; i < saying.length; i++) {
    let character = saying.charAt(i);
    if (vowels.indexOf(character) == -1){
        console.log(character + "n'est pas une voyelle");
        continue;
    }
    vowelsCounter++;
}
console.log("Il y a " + vowelsCounter + " voyelles");
printSeperator();

// Boucle infinie et instruction "break"
while(true) {
    randomNbr = getRandomNumber();

    if(randomNbr === 8) {
        break;
    }
    console.log(randomNbr + " n'est pas le chiffre mystère");
}
console.log("8 est bien notre chiffre mystère");
printSeperator();

// Accéder à tous les liens <a> de la page
let links = document.getElementsByTagName("a");
console.log(links);
console.log(links[5]);
console.log(links.item(7));

// Modifier le style d'un paragraphe avec JavaScript
let jsDom1 = document.getElementById("jsDom1");
jsDom1.innerHTML = "JavaScript peut modifer le contenu du paragraphe";

// Modifier le style avec JavaScript
let jsDom2 = document.getElementById("jsDom2");
jsDom2.style.color = "green";
jsDom2.style.fontSize = "130%";
jsDom2.style.fontWeight = "bold";

// Spécifier une masse CSS pour jsDom3
let jsDom3 = document.getElementById("jsDom3");
jsDom3.setAttribute("class", "js_dom3");

// Créer un paragraphe et son contenu avec JavaScript
const jsDom4 = document.createElement("p");
//jsDom4.innerHTML = "aaaaaaaaaaaaaaaaaaaaaaaaaa"; aurait suffit
const content4 = document.createTextNode("Contenu temporaire");
jsDom4.appendChild(content4);
const main = document.getElementById("main");
main.appendChild(jsDom4);
jsDom4.innerHTML = "JavaScript peut créer des balises et du contenu";

// Créer un bouton de basculement "cacher/montrer" avec JavaScript
const toggleBtn = document.createElement("button");
main.appendChild(toggleBtn);
toggleBtn.setAttribute("class", "js_dom5");
toggleBtn.innerHTML = "cacher";
toggleBtn.onclick = function() {hideParagraph("jsDom3")}; 

function hideParagraph(id) {
    let paragraph = document.getElementById(id);
    if (paragraph.style.display === "none") {
        paragraph.style.display = "block";
        toggleBtn.innerHTML = "cacher";
    } else {
        paragraph.style.display = "none";
        toggleBtn.innerHTML = "montrer";
    }
}



